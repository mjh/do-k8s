FROM gitpod/workspace-full

# This is horrible container optimization BUT it does make it easier to read commands

# Installs the Digital Ocean CLI
RUN brew install doctl

# Installs tfenv (so that it can test various TF deployments)
RUN brew install tfenv

# Installs kubectl
RUN brew install kubectl

# Installs Helm
RUN brew install helm

# Installs k9s
RUN brew install derailed/k9s/k9s