# Kubernetes Labs on DigitalOcean with Gitpod

Run basic labs for Kubernetes by leveraging DigitalOcean and never leaving your browser.

## Gitpod

[![Open in Gitpod](https://gitpod.io/button/open-in-gitpod.svg)](https://gitpod.io/#https://gitlab.com/mjh/do-k8s)

Although you could pull this down and install things and run locally, this is designed to be ran in Gitpod so that you don't need to worry about your machine.

## Prerequisites

Before beginning, make sure you have a Gitpod environment variable called "DIGITALOCEAN_ACCESS_TOKEN" that includes your [DO Access Token](https://cloud.digitalocean.com/account/api/tokens). You can should supply this directly in [Gitpod's dashboard](https://gitpod.io/variables) or the good ole `export DIGITALOCEAN_ACCESS_TOKEN=APIKEY`

Don't have a DigitalOcean account? [Click Here for my referral link](https://m.do.co/c/b0719d610a69) for $100 in credits - way more than you need to do these labs!

## Labs

### Start it up!

This'll let you build up and tear down a k8s cluster on DigitalOcean that's 3 nodes on the smallest droplet size.

1. You'll need to initialize it by going to `/workspace/do-k8s/terraform` and running `terraform init`
1. Then run `terraform apply` in the same directory - it'll use some defaults to speed it up.
1. Type `Yes` to confirm creating the resources
1. Next, grab your kubeconfig with `doctl kubernetes cluster kubeconfig save gitpod-cluster`

### Labs

Want some examples? Okay!

#### Kubectl Commands

1. Let's make sure it runs with `kubectl get nodes`. This will show you all your nodes that your k8s setup has.
1. Take a look at your pods in your default namespace with `kubectl get po` (or `kubectl get pods`)
1. Default namespace? Look at all your namespaces with `kubectl get ns`
1. What about all your pods? Better check it out with `kubectl get po -A`!


#### Kuma (kubectl apply)

Can I interest you in a service mesh? [Kuma](https://kuma.io) is a newer kid on the block that you can install pretty easily

1. Pull it down with `curl -L https://kuma.io/installer.sh | sh -`
1. Change directory with `cd kuma*/bin` 
1. Fire it up with `./kumactl install control-plane | kubectl apply -f -`
1. Run `kubectl get pod -n kuma-system` and wait for it to show running
1. Port forward with `kubectl port-forward svc/kuma-control-plane -n kuma-system 5681:5681`
1. Click the "Open in browser" alert Gitpod gives you and then append `/gui` to the url (e.g. https://5682-black-cod-lh3kv8nb.ws-us17.gitpod.io/gui/) and enjoy poking around with Kuma!

Done with Kuma? Make your choice!
1. Delete just Kuma off your cluster with `./kumactl install control-plane | kubectl delete -f -` (notice the delete instead of apply!)
1. Destroy your whole cluster with `terraform destroy` and answering `yes` (and start a new one if you want with `terraform apply`!)


#### Gitpod-ception (Helm)

Installing Gitpod...while using Gitpod? Let's do this!

This is a more expensive task - it requires stronger droplets to run. If you let this run, it can be $120/mo, so be sure to destroy this (with `terraform destroy`!). However, they bill by the minute, so a couple of hours of this will stay under a cup of coffee.

Also, if you don't control your own domain, this will be impossible to complete, but you can make it almost all the way through.

1. Update to more powerful nodes, by going to `/workspace/do-k8s/terraform` and running `terraform apply -var=gitpod.tfvars`
1. Wait until Terraform is done, about 10 minutes - this will destroy and rebuild your cluster
1. Get a Kubeconfig file with `doctl kubernetes cluster kubeconfig save gitpod-cluster`
1. Head over to `/workspace/do-k8s/gitpod-values/`
1. Update `values.custom.yaml` with your own generated secrets
1. Create a namespace by running `kubectl apply -f gitpod-ns.yaml`
1. Run `helm install -n gitpod-server -f values.custom.yaml gitpod gitpod.io/gitpod --version=0.10.0`
1. Get cozy - it will take a bit to get it all running. You can do `kubectl get po -n gitpod-server` after the Helm is done - but it takes about 10 minutes all together to get fired up.
1. If you run into issues, which happens, consult the [Gitpod Self-Install Troubleshooting Guide](https://www.gitpod.io/docs/self-hosted/latest/troubleshooting). Especially if it seems to be taking significantly longer than anticipated!
1. From here, the install is there, but there's a few [extra steps](https://www.gitpod.io/docs/self-hosted/latest/configuration/ingress) you need to take with a domain you own. I encourage you to follow through if you have your own domain! Otherwise, you can skip down to "All done?"
1. After you do your domain setup, you just follow the prompts at your domain (on https) and you are off to the races.


All done?

1. Delete just Gitpod buy running `helm delete -n gitpod-server gitpod`
1. Destroy your whole cluster by going to `/workspace/do-k8s/terraform` and running `terraform destroy` and answering `yes` (and start a new one if you want with `terraform apply`!)

#### Metrics

Boy howdy, do I love metrics. And you should too! 

Kubernetes ships with limited metrics but has the magic known as Metrics Server.
Let's get it going!

1. Run `kubectl apply -f https://github.com/kubernetes-sigs/metrics-server/releases/latest/download/components.yaml`
1. Use commands such as `kubectl top pod` to see resource usage of pods
1. Try `kubectl top node` to see the same for nodes

Okay, yes, that's cool for people who love command line and Linux from 1991.
But what if you want something a little easier to run?
Let me introduce you to a tool called [k9s](https://k9scli.io/).

K9s is still command line but instead of it requiring a knowledge of many kubernetes commands, it bundles them and makes them a lot easier to navigate through. 
Wouldn't you know this lab contains it? 
Let's take it for a drive with VMWare's demo application, [Yelb](https://github.com/mreferre/yelb).

##### Deploy Yelb Demo App

1. Go to `/workspace/do-k8s/k9s`
1. Create the namespace for our demo Yelb app by running `kubectl apply -f yelb-ns.yaml`
1. Install Yelb by running `kubectl apply -f https://raw.githubusercontent.com/lamw/vmware-k8s-app-demo/master/yelb-lb.yaml`
1. Run `kubectl -n yelb get pods` and make sure the status for everything is `Running`
1. Wait for a LoadBalancer to be ready by checking `kubectl -n yelb get svc/yelb-ui`. When you see something under `EXTERNAL-IP` go to that IP after http:// (instead of https://) - about 10 minutes.

This brings up the Yelb app, which is pretty sweet for testing things. 

##### K9s

With Yelb running

1. Type `k9s`
1. Hit `0` to bring up all your pods. There will be quite a few! But scroll down to `yelb-appserver-` and hit enter.
1. You can see a lot of good information here now! How much CPU and Memory, the image it's from, and all kinds of useful troubleshooting things.
1. Go click in the application itself from the URL you opened above. (You might have to click several times)
1. Watch in real time as the appserver goes up and down in usage!
1. You can hit escape (after clicking in terminal) to go back and poke at all other other pods.
1. This also gives you info on the cluster itself, such as the name, Kubernetes version, and other useful troubleshooting tips.
1. Use Ctl+C to get out of k9s

##### Remove Yelb 

To remove Yelb, just run the apply commands in reverse, but replace `apply` with `delete`
1. `kubectl delete -f https://raw.githubusercontent.com/lamw/vmware-k8s-app-demo/master/yelb-lb.yaml`
1. `kubectl delete -f yelb-ns.yaml`

### Tear it all down

Before logging out of Gitpod, be sure to go to `/workspace/do-k8s/terraform` and run `terraform destroy`! Since this is only for testing and not production usage, state is only held in a gitpod workspace. If you forget to run `terraform destroy` it'll leave the cluster running until you shut it down through the [Digital Ocean Console](https://cloud.digitalocean.com/kubernetes/clusters)